/**
 * @author sumyathlaing
 */

package orderexercise;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {	

	@RequestMapping(value = "/")
	public ModelAndView main() {
		return new ModelAndView("redirect:/login");
	}	

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		return new ModelAndView("login");
	}
	
	@RequestMapping(value="/fail2login", method = RequestMethod.GET)
	 public String loginerror(ModelMap model) {	 
	  model.addAttribute("error", "true");
	  return "login";	 
	 }
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	 public String logout(ModelMap model) {	 
	  return "login";	 
	 }
}
