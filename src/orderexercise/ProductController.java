/**
 * @author sumyathlaing
 */

package orderexercise;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import au.com.bytecode.opencsv.CSVReader;

@Controller
public class ProductController {
	@Autowired
	ProductDAL productDal;
	List<Product> productList;
	ArrayList<String> row;
	String name;
	int price;
	Product product = new Product();
	
	
	JSONObject json = new JSONObject();

	@Autowired
	@Qualifier("formValidation")
	private Validator validator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(Integer.class, new CustomNumberEditor(Integer.class, true));
	}

	@ModelAttribute("product")
	public Product getProduct() {
		return new Product();
	}

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public ModelAndView product(@RequestParam(required = false) Integer page, Principal principal) {

		ModelAndView modelView = new ModelAndView("product");

		String name = principal.getName();
		modelView.addObject("author", name);
		
		JSONArray jArray = new JSONArray();
		
		
		productList = new ArrayList<>();
		try {
			productList = productDal.getAll();
			for (int i = 0; i < productList.size(); i++) {
				
				JSONObject obj = new JSONObject();
				obj.put("id", productList.get(i).getId());
				obj.put("name", productList.get(i).getName());
				obj.put("price", productList.get(i).getPrice());
				jArray.put(obj);			
			}
			
			modelView.addObject("productLists", jArray.toString());
			
		} catch (ClassNotFoundException | SQLException | JSONException e) {
			e.printStackTrace();
		}

		PagedListHolder<Product> pagedListHolder = new PagedListHolder<>(productList);
		pagedListHolder.setPageSize(5);

		modelView.addObject("maxPages", pagedListHolder.getPageCount());

		if (page == null || page < 1 || page > pagedListHolder.getPageCount())
			page = 1;
		modelView.addObject("page", page);

		if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(0);
			modelView.addObject("productList", pagedListHolder.getPageList());
		} else if (page <= pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(page - 1);
			modelView.addObject("productList", pagedListHolder.getPageList());
		}

		return modelView;
	}

	@RequestMapping(value = "/productadd", method = RequestMethod.GET)
	public ModelAndView addProduct() {
		ModelAndView modelView = new ModelAndView("addproduct", "command", new Product());
		return modelView;
	}

	@RequestMapping(value = "/productadd", method = RequestMethod.POST)
	public ModelAndView addproduct(@ModelAttribute("product") @Validated Product product, BindingResult result,
			ModelMap model) {

		if (result.hasErrors()) {
			return new ModelAndView("addproduct");
		}

		try {
			productDal.add(product);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return new ModelAndView("redirect:/product");
	}

	@RequestMapping(value = "/productdelete", method = RequestMethod.GET)
	public ModelAndView deleteProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
		int id = Integer.parseInt(request.getParameter("id"));
		Product product = findProductById(id);
		productDal.delete(product);
		return new ModelAndView("redirect:/product");
	}

	@RequestMapping(value = "/productedit", method = RequestMethod.POST)
	public ModelAndView editproduct(@ModelAttribute("editproduct") @Validated Product product, BindingResult result,
			ModelMap model) {

		if (result.hasErrors()) {
			return new ModelAndView("editproduct");
		}

		try {
			productDal.update(product);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/product");
	}

	@RequestMapping(value = "/productedit", method = RequestMethod.GET)
	public ModelAndView editProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {

		int id = Integer.parseInt(request.getParameter("id"));

		Product product = findProductById(id);

		ModelAndView modelView = new ModelAndView("editproduct", "command", product);
		return modelView;
	}

	@RequestMapping(value = "/productdownloadCSV", method = RequestMethod.GET)
	public void doDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String csvFileName = "product.csv";
		response.setContentType("text/csv");
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);

		// get absolute path of the application
		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		System.out.println("appPath = " + appPath);
		response.setHeader(headerKey, headerValue);

		productList = new ArrayList<>();
		try {
			productList = productDal.getAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		row = new ArrayList<>();
		for (Product p : productList) {
			row.add(String.valueOf(p.getId()));
			row.add(",");
			row.add(p.getName());
			row.add(",");
			row.add(String.valueOf(p.getPrice()));
			row.add("\n");
		}

		Iterator<String> iterator = row.iterator();
		while (iterator.hasNext()) {
			String datas = iterator.next();
			response.getOutputStream().print(datas);
		}
		response.getOutputStream().flush();
	}

	@RequestMapping(value = "/productimportCSV", method = RequestMethod.GET)
	public ModelAndView uploadFile() {
		ModelAndView modelView = new ModelAndView("uploadfile");
		return modelView;
	}

	@RequestMapping(value = "/productimportCSV", method = RequestMethod.POST)
	public String uploadFile(ModelMap model, @RequestParam("file") MultipartFile file, HttpServletRequest request) {

		if (file.isEmpty()) {
			model.put("msg", "failed to upload file because its empty");
			return "uploadfile";
		}

		String rootPath = request.getSession().getServletContext().getRealPath("/");
		File dir = new File(rootPath + File.separator + "uploadedfile");
		if (!dir.exists()) {
			dir.mkdirs();
		}

		File serverFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());

		System.out.println("File Path " + serverFile);

		try {
			try (InputStream is = file.getInputStream();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile))) {
				int i;
				// write file to server
				while ((i = is.read()) != -1) {
					stream.write(i);
				}
				stream.flush();
			}
		} catch (IOException e) {
			model.put("msg", "failed to process file because : " + e.getMessage());
			return "uploadfile";
		}

		String[] nextLine;
		try {

			try (FileReader fileReader = new FileReader(serverFile); CSVReader reader = new CSVReader(fileReader);) {

				while ((nextLine = reader.readNext()) != null) {
					for (int i = 0; i < nextLine.length; i += 2) {
						name = nextLine[i];
						product.setName(name);
					}
					for (int j = 1; j < nextLine.length; j += 2) {
						price = Integer.valueOf(nextLine[j]);
						product.setPrice(price);
					}
					productDal.add(product);
					productDal.getAll();
				}
			} catch (ClassNotFoundException | SQLException e) {
				model.put("msg", "failed to process file because : " + e.getMessage());
				return "uploadfile";
			}
		} catch (IOException e) {
			model.put("msg", "error while reading csv and put to db :" + e.getMessage());
			return "uploadfile";
		}
		return "redirect:/product";
	}

	private Product findProductById(int id) {
		Product product = new Product();
		for (Product p : productList) {
			if (p.getId() == id) {
				product.setId(p.getId());
				product.setName(p.getName());
				product.setPrice(p.getPrice());
			}
		}
		return product;
	}

}
