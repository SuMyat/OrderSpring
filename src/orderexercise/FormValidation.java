/**
 * @author sumyathlaing
 */

package orderexercise;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class FormValidation implements Validator {

	private Pattern pattern;
	private Matcher matcher;

	String STRING_PATTERN = "[a-zA-Z]+";
	String PRICE_PATTERN = "[0-9]+";

	@Override
	public boolean supports(Class<?> classes) {
		return classes.equals(Product.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Product product = (Product) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required.name");
		if (product.getName() != null && !(product.getName().isEmpty())) {
			pattern = Pattern.compile(STRING_PATTERN);
			matcher = pattern.matcher(product.getName());
			if (!matcher.matches()) {
				errors.rejectValue("name", "name.containNonChar");
			}
			if (product.getName().length() > 20) {
				errors.rejectValue("name", "name.length");
			}
		}

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "required.price");
		if (product.getPrice() != 0) {
			pattern = Pattern.compile(PRICE_PATTERN);
			matcher = pattern.matcher(String.valueOf(product.getPrice()));
			if (!matcher.matches()) {
				errors.rejectValue("price", "price.incorrect");
			}

		}
	}
}
