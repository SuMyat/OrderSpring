<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Upload File</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/jsp/inc/header.jsp"%>
	<%@ include file="/WEB-INF/jsp/inc/css.jsp"%>
	<%@ include file="/WEB-INF/jsp/inc/menu.jsp"%>
	<br>
	<div class="container">
		<h2>
			<p class="w3-teal" align="center">Upload File</p>
		</h2>
		<br>

		<form:form class="form-horizontal" method="POST"
			action="/OrderSpring/productimportCSV" enctype="multipart/form-data">

			<div class="form-group">
				<label class="col-sm-2 control-label">Upload File</label>
				<div class="col-sm-5">
					<input type="file" class="form-control" name="file"></input>
				</div>
				<font color="red"><label class="col-sm-5 control-label">${msg}</label>
				</font>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Upload</button>
				</div>
			</div>

		</form:form>
	</div>
</body>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp"%>
</html>