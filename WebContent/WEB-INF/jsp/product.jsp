<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head data-gwd-animation-mode="quickMode">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

</head>
<body>
	<%@ include file="/WEB-INF/jsp/inc/header.jsp"%>
	<%@ include file="/WEB-INF/jsp/inc/css.jsp"%>
	<%@ include file="/WEB-INF/jsp/inc/menu.jsp"%>
	<br>
	<div class="container">
		<a class="btn btn-default btn-sm active"
			href="http://localhost:8080/OrderSpring/productadd" role="button">New</a>
		<a class="btn btn-default btn-sm active"
			href="http://localhost:8080/OrderSpring/productdownloadCSV"
			role="button">Download</a> <a class="btn btn-default btn-sm active"
			href="http://localhost:8080/OrderSpring/productimportCSV"
			role="button">Import CSV</a>
	</div>
	<br>
	<div class="container" ng-app="sortApp" ng-controller="mainController">
		<form>
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-addon">
						<i class="fa fa-search"></i>
					</div>
					<input type="text" class="form-control" placeholder="Search"
						ng-model="searchProduct">
				</div>
			</div>
		</form>
		<table class="table table-bordered table-striped">
			<thead>
				<tr class="w3-teal">
					<td><a href="#"
						ng-click="sortType = 'id'; sortReverse = !sortReverse"> ID </a></td>
					<td><a href="#"
						ng-click="sortType = 'name'; sortReverse = !sortReverse"> Name
					</a></td>
					<td><a href="#"
						ng-click="sortType = 'price'; sortReverse = !sortReverse">
							Price </a></td>
					<td></td>
					<td></td>
				</tr>
			</thead>
			<tbody>
				<tr
					ng-repeat="roll in product | orderBy:sortType:sortReverse | filter:searchProduct | startFrom:currentPage*pageSize | limitTo:pageSize ">
					<td>{{ roll.id }}</td>
					<td>{{ roll.name }}</td>
					<td>{{ roll.price }}</td>
					<td><a class="btn btn-default"
						href="http://localhost:8080/OrderSpring/productedit?id={{roll.id}}"
						role="button">Edit</a></td>
					<td><a class="btn btn-default"
						href="http://localhost:8080/OrderSpring/productdelete?id={{roll.id}}"
						role="button">Delete</a></td>
				</tr>
			</tbody>
		</table>


		<button ng-disabled="currentPage == 0"
			ng-click="currentPage=currentPage-1">Previous</button>
		{{currentPage+1}}/{{numberOfPages()}}
		<button ng-disabled="currentPage >= getData().length/pageSize - 1"
			ng-click="currentPage=currentPage+1">Next</button>
		<br> <br>
</body>
<%@ include file="/WEB-INF/jsp/inc/footer.jsp"%>
</html>

<script>

var app = angular.module('sortApp', [])
app.controller('mainController', ['$scope', '$filter', function ($scope, $filter) {
  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.searchProduct   = '';  
  $scope.product = ${productLists}; 
  $scope.sortType     = 'id';
  $scope.sortReverse  = false;
  
  $scope.getData = function () {   
      return $filter('filter')($scope.product, $scope.searchProduct);
  }
  $scope.numberOfPages=function(){
      return Math.ceil($scope.getData().length/$scope.pageSize);                
  }
}]);

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; 
        return input.slice(start);
    }
});
</script>

