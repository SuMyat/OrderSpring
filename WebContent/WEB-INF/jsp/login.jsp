<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>User Login</title>
<style>
.errorblock {
	color: #ff0000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
}
</style>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body onload='document.f.j_username.focus();'>
	<%@ include file="/WEB-INF/jsp/inc/header.jsp"%>
	<br>
	<div class="container">
		<c:if test="${not empty error}">
			<div class="errorblock">
				Your login attempt was not successful, try again.<br /> Caused :
				${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
			</div>
		</c:if>
	</div>
	<br>
	<div class="col-sm-6 col-md-4 col-md-offset-4">
		<h2 align="center">Sign in to continue</h2>
	</div>
	<br>

	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<form class="form-signin" name='f'
					action="<c:url value='j_spring_security_check' />" method='POST'>
					<input type='text' name='j_username' value='' class="form-control"
						placeholder="UserName" required autofocus> <br> <input
						type='password' name='j_password' class="form-control"
						placeholder="Password" required /> <br>
					<button class="btn btn-lg btn-primary btn-block" type="submit">
						Sign in</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

